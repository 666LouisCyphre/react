import React from 'react'
import FadeIn from 'react-fade-in'
import {Link} from 'react-router-dom'
import Graph from './graph'

import './plot.css'

export default class PlotPage extends React.Component {
    render() {
        return (
            <FadeIn delay='500'>
            <main className='plotContainer'>
                <Graph />
            </main>
            <div className='btns'>
                <Link to='/download'><button className='ddBtns'>Download</button></Link>
                <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9KKWSY36RAKLL&source=url'>
                    <button className='ddBtns'>Donate</button>
                </a>
            </div>

            </FadeIn>
        )
    }
}