import React from 'react'
import FadeIn from 'react-fade-in'
import {Link} from 'react-router-dom'

export default class DownlaodPage extends React.Component {
    render() {
        return(
            <FadeIn delay='500'>
                <div>Currently Unavailable</div>
                <Link to='/plot'><button className='startBtn'>Go back</button></Link>
            </FadeIn>
        )
    }
}