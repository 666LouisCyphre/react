import React from 'react'
import './home.css'
import FadeIn from 'react-fade-in'
/*TODO: create react loading spinner */
/*TODO: make redirect without reloading after clicking button */
/*in footer should be icons like github, author, bitbucket */
import {Link} from 'react-router-dom'

class HomePage extends React.Component {

    render() {
        return (
            <main className='homeContainer'>
                <FadeIn delay='500'>
                    <div className='helloText'>Wanna start investing in currency?</div>
                    <div className='centre'>
                        <Link to='/intro'><button className='startBtn'>Let's Go!</button></Link>
                    </div>
                </FadeIn>
            </main>
        )
    }
}

export default HomePage