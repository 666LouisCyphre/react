import React from 'react'
import Chart from 'chart.js'
import ClipLoader from "react-spinners/ClipLoader"

import './graph.css'
let myLineChart

Chart.defaults.global.elements.line.tension = 0;

class LineGraph extends React.Component {
    chartRef = React.createRef()

    componentDidMount() {
        this.renderChart()
    }
    componentDidUpdate() {
        this.renderChart()
    }
    renderChart = () => {
        const myChartRef = this.chartRef.current.getContext("2d");
        const {labels, data} = this.props;

        if (typeof myLineChart !== "undefined") myLineChart.destroy();

        myLineChart = new Chart(myChartRef, {
            type: "line",
            data: {
                labels: labels,
                datasets: data
            },
            options: {
                responsive: true
            }
        });
    }
    render() {
        return (
            <div className='graphContainer'>
                <canvas
                    id="myChart"
                    ref={this.chartRef}
                />
            </div>
        )
    }
}

export default class Graph extends React.Component {
    state = {
        data: [],
        loading: false
    }

    componentDidMount() {
        const data = [
            { label: 'GBP', fill: false, data: getRate('gbp') },
            { label: 'USD', fill: false, data: getRate('usd') },
            { label: 'AUD', fill: false, data: getRate('aud') },
            { label: 'NOK', fill: false, data: getRate('nok') },
            { label: 'EUR', fill: false, data: getRate('eur') }
        ]
        this.setState({data: data})
    }

    normalChart = () => {
        //myLineChart.destroy()
        const data = [
            { label: 'GBP', fill: false, data: getRate('gbp') },
            { label: 'USD', fill: false, data: getRate('usd') },
            { label: 'AUD', fill: false, data: getRate('aud') },
            { label: 'NOK', fill: false, data: getRate('nok') },
            { label: 'EUR', fill: false, data: getRate('eur') }
        ]
        this.setState({data: data})
    }

    updateChart = () => {
        //myLineChart.destroy()
        const data = [
            { label: 'GBP', fill: false, data: getRate('gbp') },
            { label: 'USD', fill: false, data: getRate('usd') },
            { label: 'EUR', fill: false, data: getRate('eur') }
        ]
        this.setState({data: data})
    }

    render() {
        return (
        <div>
        <div className="sweet-loading">
            <ClipLoader
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
            />
        </div>
        <button onClick={this.normalChart}>All Currency</button>
        <button onClick={this.updateChart}>3 Currency</button>
        <LineGraph labels={effectiveDate} data={this.state.data}/>
        </div>
        )
    }
}

let effectiveDate = [1.06, 2.06, 3.06, 4.06, 5.06]

function get(currency) {
    return new Promise(function(resolve, reject) {
        const xhr = new XMLHttpRequest()

        xhr.responseType = "json"

        xhr.open("GET", `https://api.nbp.pl/api/exchangerates/rates/a/${currency}/2020-06-01/2020-06-07/`, true)

        xhr.onload = function() {
            if (xhr.status === 200) {
                resolve(xhr.response)
            }
            else {
                reject(Error(xhr.statusText))
            }
        }

        xhr.onerror = function() {
            reject(Error("Network Error"))
        }

        xhr.send()
    })
}

//https://web.dev/promises/
function getRate(currency) {
    let data = []

    get(currency).then(function(response) {
        for(let i = 0; i < response.rates.length; ++i) {
            data.push(response.rates[i].mid)
        }

    }, function(error) {
        console.error("error", error)
    })
    return data
}