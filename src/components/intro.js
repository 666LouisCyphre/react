import React from 'react'
import './intro.css'
import {Link} from 'react-router-dom'
import FadeIn from 'react-fade-in'

export default class IntroPage extends React.Component {
    render() {
        return (
            <main className='introContainer'>
                <FadeIn delay='500'>
                <div className='introText'>
                    In a moment you'll see graph, which depicts currency rates in PLN for:<br/>
                    GBP, USD, AUD, EUR, NOK.
                    <br/><br/>
                    Remember though, this 'game' is risky. Invest wisely!  
                </div>
                <Link to='/plot'><button className='startBtn readyBtn'>Ready?</button></Link>
                </FadeIn>
            </main>
        )
    }
}