import React from 'react';

import HomePage from './components/home'
import IntroPage from './components/intro'
import PlotPage from './components/plot'
import DownloadPage from './components/download'

import {
  BrowserRouter as Router,
  //Switch,
  Route
} from "react-router-dom";
import { AnimatedSwitch } from 'react-router-transition';

function App() {
  return (
    <Router>
    <AnimatedSwitch atEnter={{ opacity: 0 }} atLeave={{ opacity: 0 }} atActive={{ opacity: 1 }} className="switch-wrapper">
      <Route exact path='/'>
           <HomePage />
         </Route>
         <Route path='/intro'>
           <IntroPage />
         </Route>
         <Route path='/plot'>
           <PlotPage />
         </Route>
         <Route path='/download'>
            <DownloadPage />
         </Route>
    </AnimatedSwitch>
    </Router>
  );
}

export default App;
